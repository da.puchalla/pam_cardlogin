CC      = /usr/bin/gcc
PKGCONF = /usr/bin/pkg-config
CFLAGS  = -Wall -O3


all: pam_cardlogin.so cardlogin_cardadmin

pam_cardlogin.so: pam_cardlogin.o get_current_card_id.o
	$(CC) -shared -fpic $(CFLAGS) $$($(PKGCONF) --libs libpcsclite) -o pam_cardlogin.so pam_cardlogin.o get_current_card_id.o

cardlogin_cardadmin: cardlogin_cardadmin.o get_current_card_id.o
	$(CC) $(CFLAGS) -s $$($(PKGCONF) --libs libpcsclite) -o cardlogin_cardadmin cardlogin_cardadmin.o get_current_card_id.o

pam_cardlogin.o: pam_cardlogin.c
	$(CC) $(CFLAGS) -fpic -c pam_cardlogin.c

get_current_card_id.o: get_current_card_id.c
	$(CC) $(CFLAGS) -fpic $$($(PKGCONF) --cflags libpcsclite) -c get_current_card_id.c

cardlogin_cardadmin.o: cardlogin_cardadmin.c
	$(CC) $(CFLAGS) -c cardlogin_cardadmin.c

clean:
	rm -f pam_cardlogin.so cardlogin_cardadmin pam_cardlogin.o get_current_card_id.o cardlogin_cardadmin.o

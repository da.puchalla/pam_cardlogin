/* pam_CARDlogin - modules for PAM system using DESFire card id for auth
 *
 * config.h - compile-time configuration values
 *
 * Copyright (C) 2010  Frank Engler
 *
 * This file is part of pam_CARDlogin.
 *
 * Pam_CARDlogin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pam_CARDlogin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pam_CARDlogin.  If not, see <http://www.gnu.org/licenses/>.
 */

#define LOGNAME "pam_CARDlogin"	              /* name for log-file entries */
#define CARDLOGIN_USER_FILE "/etc/security/CARDlogin"     /* file to store
							   * card uids */
#define CARDLOGIN_NEW_USER_FILE "CARDlogin.XXXXXX"        /* file to store
							   * new card uids */
#define LINE_MAX 128   /* max size of each line in CARDLOGIN_USER_FILE */
#define WAIT_FOR_CARD 5000 /* ms to wait for a card inserted */

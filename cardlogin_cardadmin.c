/*
 *  card administration tool for PAM CARDlogin Module 
 *  Copyright (C) 2010 Frank Engler <pam_CARD@web.de>,
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This program is based on pam_p11 by Mario Strasser <mast@gmx.net>
 * and on the sample program to use PC/SC API by Ludovic Rousseau
 * <ludovic.rousseau@free.fr>.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <PCSC/wintypes.h>
#include "get_current_card_id.h"
#include "licence.h"
#include "config.h"

int main(int argc, const char **argv)
{
  char *user = NULL, *cardlogin_new_user_filename = NULL;
  int fd;
  FILE *file;
  int reader_number;
  BYTE *card_id;

  /* GPL stuff */
  printf(PROGRAMM);
  if (argc > 1 && (argv[1][0] == 'w' || argv[1][0] == 'W') && argv[1][1] == '\0') {
    printf(WARRANTY);
    return EXIT_SUCCESS;
  }
  if (argc > 1 && (argv[1][0] == 'l' || argv[1][0] == 'L') && argv[1][1] == '\0') {
    printf(LICENCE, argv[0]);
    return EXIT_SUCCESS;
  }
  if (argc > 1 && (argv[1][0] == 'g' || argv[1][0] == 'G') && argv[1][1] == '\0') {
    printf(GPL);
    return EXIT_SUCCESS;
  }
  printf(DISCLAIMER, argv[0], argv[0]);
	
  /* test arguments */
  if (argc!=3) {
    printf("wrong options:\n syntax: %s <reader number> <username>", argv[0]);
    printf("\nIf you have only one card reader the number is 0\n");
    return 1;
  }
  reader_number = atoi(argv[1]);
  user = strdup(argv[2]);

  /* get mapping-file */
  asprintf(&cardlogin_new_user_filename, "%s/%s", P_tmpdir,
	   CARDLOGIN_NEW_USER_FILE);
  umask(S_IXUSR | S_IRWXG | S_IRWXO);
  fd = mkstemp(cardlogin_new_user_filename);
  if (fd == -1) {
    printf("fatal: cannot open file %s", cardlogin_new_user_filename);
    return EXIT_FAILURE;
  }
  file = fdopen(fd, "w");
  if (file == NULL) {
    printf("fatal: cannot open mapping-file %s", CARDLOGIN_NEW_USER_FILE);
    return EXIT_FAILURE;
  }
  
  if (get_current_card_id (NULL, NULL, reader_number,
			   (const BYTE **) &card_id) == EXIT_SUCCESS) {
    /* don't uses status bytes for card_id, cut them off */
    card_id[14] = '\0';
    fprintf(file,"%s:%14s\n", user, card_id);
    fclose(file);
    printf("Card connected to user %s\n", user);
    printf("To activate changes do \'cat -n %s >> %s\'\n", 
	   cardlogin_new_user_filename, CARDLOGIN_USER_FILE);
    return EXIT_SUCCESS;
  }
  return EXIT_FAILURE;
}

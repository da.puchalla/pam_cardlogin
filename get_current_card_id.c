/*
 *  get_current_card_id is part of PAM CARDlogin Module 
 *  Copyright (C) 2010 Frank Engler <pam_CARD@web.de>,
 *
 *  This library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This library is based on pam_p11 by Mario Strasser <mast@gmx.net>
 * and on the sample program to use PC/SC API by Ludovic Rousseau
 * <ludovic.rousseau@free.fr>.
 */

#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <PCSC/wintypes.h>
#include <PCSC/winscard.h>
#include <security/pam_modules.h>
#ifdef HAVE_SECURITY_PAM_EXT_H
#include <security/pam_ext.h>
#else
#define pam_syslog(handle, level, msg...) syslog(level, ## msg)
#endif
#include "DESFire.h"
#include "config.h"

#define system_message(handle, level, message...) \
  { if (handle == NULL)                           \
      printf(message);                            \
    else                                          \
      pam_syslog(handle, level, message); }

static struct pam_message msg;
static struct pam_response *resp;
static struct pam_message *(msgp[1]);
#define user_message(conversation, message)		      \
  { if (conversation == NULL)				      \
      printf("%s\n", message);				      \
    else {						      \
      msg.msg_style = PAM_TEXT_INFO;			      \
      msg.msg = strdup(message);			      \
      msgp[0] = &msg;			                      \
      conversation->conv(1, (const struct pam_message **)msgp,\
                             &resp, conversation->appdata_ptr); }}

/* We try to leave things as clean as possible */
#define release_context(handle, context)			         \
  { rv = SCardReleaseContext(context);                                   \
    if (rv != SCARD_S_SUCCESS)                                           \
      system_message(handle, LOG_ERR, "SCardReleaseContext: %s (0x%lX)", \
		     pcsc_stringify_error(rv), rv); }

static long get_reader(pam_handle_t *pamh, struct pam_conv *conv, 
		       const SCARDCONTEXT hContext,  const int reader_number,
		       const char **reader);

static long connect_card(pam_handle_t *pamh, struct pam_conv *conv, 
			 const SCARDCONTEXT hContext, const char *reader,
			 SCARDHANDLE *hCard, SCARD_IO_REQUEST **pioSendPci);

long get_current_card_id (pam_handle_t *pamh, struct pam_conv *conv, 
			  const int reader_number, const BYTE **card_id)
{
  int i;
  long rv;
  SCARDCONTEXT hContext;
  SCARDHANDLE hCard;
  const char *reader = NULL;
  SCARD_IO_REQUEST *pioSendPci;
  SCARD_IO_REQUEST pioRecvPci;
  static BYTE pbRecvBuffer[16];
  DWORD dwSendLength, dwRecvLength;

  /* establish pcsc context */
  rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL,
			     &hContext);
  if (rv != SCARD_S_SUCCESS) {
    system_message(pamh, LOG_ERR,
		   "SCardEstablishContext: Cannot Connect to Resource Manager %lX", rv);
    return PAM_AUTHINFO_UNAVAIL;
  }

  /* find reader */
  rv = get_reader(pamh, conv, hContext, reader_number, &reader);
  if (rv != 0)
    return rv;  /* error message already done; forward rv */
  
  /* connect to card */
  rv = connect_card(pamh, conv, hContext, reader, &hCard, &pioSendPci);
  if (rv != 0)
    return rv;   /* error message already done; forward rv */

  /* get card ID */
  dwSendLength = sizeof(SmartcardCommandGetVersion);
  dwRecvLength = sizeof(pbRecvBuffer);
  rv = SCardTransmit(hCard, pioSendPci, SmartcardCommandGetVersion,
		     dwSendLength, &pioRecvPci, pbRecvBuffer,
		     &dwRecvLength);
  for (i=0; i<=1; i++) {
    if (rv != SCARD_S_SUCCESS || 
	pbRecvBuffer[dwRecvLength-1] != 0xAF) {
      system_message(pamh, LOG_ERR, "Error communicating with card");
      rv = SCardDisconnect(hCard, SCARD_UNPOWER_CARD);
      release_context(pamh, hContext);
      return PAM_AUTHINFO_UNAVAIL;
    }
    dwSendLength = sizeof(SmartcardCommandFetchMoreData);
    dwRecvLength = sizeof(pbRecvBuffer);
    rv = SCardTransmit(hCard, pioSendPci, SmartcardCommandFetchMoreData,
		       dwSendLength, &pioRecvPci, pbRecvBuffer,
		       &dwRecvLength);
  }
  if (rv != SCARD_S_SUCCESS || pbRecvBuffer[dwRecvLength-1] != 0x00) {
    system_message(pamh, LOG_ERR, "Error communicating with card");
    rv = SCardDisconnect(hCard, SCARD_UNPOWER_CARD);
    release_context(pamh, hContext);
    return PAM_AUTHINFO_UNAVAIL;
  }

  /* card disconnect */
  rv = SCardDisconnect(hCard, SCARD_UNPOWER_CARD);
  release_context(pamh, hContext);
  *card_id = pbRecvBuffer;
  return 0;
}

static long get_reader(pam_handle_t *pamh, struct pam_conv *conv, 
		       const SCARDCONTEXT hContext,  const int reader_number,
		       const char **reader)
{
  long rv;
  char **readers;
  static char *tmp_reader;
  DWORD dwReaders;
  LPSTR mszReaders = NULL;
  int nbReaders;
  char *ptr = NULL;

  /* Retrieve the available readers list.
   *
   * 1. Call with a null buffer to get the number of bytes to allocate
   * 2. malloc the necessary storage
   * 3. call with the real allocated buffer
   */
  rv = SCardListReaders(hContext, NULL, NULL, &dwReaders);
 
  mszReaders = malloc(sizeof(char)*dwReaders);
  if (mszReaders == NULL) {
    system_message(pamh, LOG_ERR, "malloc: not enough memory");
    release_context(pamh, hContext);
    return PAM_AUTHINFO_UNAVAIL;
  }

  rv = SCardListReaders(hContext, NULL, mszReaders, &dwReaders);
  
  /* Extract readers from the null separated string and get the total
   * number of readers */
  nbReaders = 0;
  ptr = mszReaders;
  while (*ptr != '\0') {
    ptr += strlen(ptr)+1;
    nbReaders++;
  }

  if (nbReaders == 0) {
    system_message(pamh, LOG_ERR, "No reader found");
    release_context(pamh, hContext);
    /* free allocated memory */
    if (mszReaders)
      free(mszReaders);
    return PAM_AUTHINFO_UNAVAIL;
  }

  /* allocate the readers table */
  readers = calloc(nbReaders, sizeof(char *));
  if (NULL == readers) {
    system_message(pamh, LOG_ERR, "Not enough memory for readers[]\n");
    release_context(pamh, hContext);
    /* free allocated memory */
    if (mszReaders)
      free(mszReaders);
    return PAM_AUTHINFO_UNAVAIL;
  }

  /* fill the readers table */
  nbReaders = 0;
  ptr = mszReaders;
  while (*ptr != '\0') {
    readers[nbReaders] = ptr;
    ptr += strlen(ptr)+1;
    nbReaders++;
  }
  tmp_reader = strdup(readers[reader_number]);
  /* free allocated memory */
  if (readers)
    free(readers);
  if (mszReaders)
    free(mszReaders);
  *reader = tmp_reader;
  return 0;
}

static long connect_card(pam_handle_t *pamh, struct pam_conv *conv, 
			 const SCARDCONTEXT hContext, const char *reader,
			 SCARDHANDLE *hCard, SCARD_IO_REQUEST **pioSendPci)
{
  long rv;
  int i, j;
  DWORD dwActiveProtocol, dwAtrLen;
  BYTE pbAtr[MAX_ATR_SIZE] = "";
  SCARD_READERSTATE rgReaderStates;

  /* check that card is inserted */
  rgReaderStates.szReader = reader;
  rgReaderStates.dwCurrentState = SCARD_STATE_UNAWARE;
  rv = SCardGetStatusChange(hContext, 1, &rgReaderStates, 1);
  if ((rgReaderStates.dwEventState & SCARD_STATE_PRESENT) == 0) {
    user_message(conv, "Please insert your smartcard...");
    do {
      rgReaderStates.dwCurrentState = rgReaderStates.dwEventState;
      rv = SCardGetStatusChange(hContext, WAIT_FOR_CARD, &rgReaderStates, 1);
    } while ((rv =! SCARD_E_TIMEOUT) && 
	     ((rgReaderStates.dwEventState & SCARD_STATE_PRESENT) == 0));
    if ((rgReaderStates.dwEventState & SCARD_STATE_PRESENT) == 0) {
      release_context(pamh, hContext);
      return PAM_AUTH_ERR;
    }
  }

  /* connect to card */
  dwActiveProtocol = -1;
  rv = SCardConnect(hContext, reader, SCARD_SHARE_EXCLUSIVE, 
		    SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, hCard, &dwActiveProtocol);
  if (rv != SCARD_S_SUCCESS) {
    system_message(pamh, LOG_ERR, "Could not connect to smartcard: %s (0x%lX)",
		   pcsc_stringify_error(rv), rv);
    release_context(pamh, hContext);
    return PAM_AUTHINFO_UNAVAIL;
  }

  /* get card ATR */
  dwAtrLen = sizeof(pbAtr);
  rv = SCardStatus(*hCard, NULL, NULL, NULL, NULL, pbAtr, &dwAtrLen);

  /* check if card is a DESFire */
  j = 0;
  for (i=0; i<dwAtrLen; i++)
    j += pbAtr[i] ^ SmartcardAtr[i];
  if (j != 0) {
    system_message(pamh, LOG_ERR, "Card is not a %s", SMARTCARD_NAME);
    rv = SCardDisconnect(*hCard, SCARD_UNPOWER_CARD);
    release_context(pamh, hContext);
    return PAM_AUTHINFO_UNAVAIL;
  }
  
  /* detect protocol */
  switch(dwActiveProtocol) {
  case SCARD_PROTOCOL_T0:
    *pioSendPci = SCARD_PCI_T0;
    break;
  case SCARD_PROTOCOL_T1:
    *pioSendPci = SCARD_PCI_T1;
    break;
  default:
    system_message(pamh, LOG_ERR, "Unknown protocol");
    rv = SCardDisconnect(*hCard, SCARD_UNPOWER_CARD);
    release_context(pamh, hContext);
    return PAM_AUTHINFO_UNAVAIL;
  }
  return 0;
}
